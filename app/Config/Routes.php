<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/Instansi', 'Home::instansi');
$routes->get('/About', 'Home::tentang');

$routes->get('/Profile', 'Home::profile');
$routes->get('/Admin', 'Admin::index');

//Admin CRUD view
$routes->get('/Create/DataAdmin', 'Admin::create');
$routes->get('/Edit/DataAdmin', 'Admin::edit');
$routes->get('/DataAdmin', 'Admin::show');

//Instansi CRUD view
$routes->get('/DataInstansi', 'Instansi::index');
$routes->get('/Create/DataInstansi', 'Instansi::create');
$routes->get('/Edit/DataInstansi', 'Instansi::edit');

$routes->get('/Instansi/Add', 'Instansi::add');

//Instansi Peserta CRUD view
$routes->get('/DataInstansiPeserta', 'Instansi::instansiPeserta');
$routes->get('/Create/DataInstansiPeserta', 'Instansi::createPeserta');
$routes->post('/Add/DataInstansiPeserta', 'Instansi::addsekolah');

$routes->get('/Edit/DataInstansiPeserta', 'Instansi::editPeserta');

//Peserta CRUD view
$routes->get('/DataPeserta', 'Peserta::show');
$routes->get('/Create/DataPeserta', 'Peserta::create');
$routes->get('/Edit/DataPeserta', 'Peserta::edit');

//repositories data
$routes->get('/Admin/Repository', 'Admin::repository');

//Penilaian
$routes->get('/Admin/Penilaian', 'Admin::penilaian');
$routes->get('/Admin/Kinerja', 'Admin::kinerja');

//Enduser Page
$routes->get('/peserta', 'Peserta::index');
$routes->get('/Peserta/Absensi', 'Peserta::absensi');

//Kinerja
$routes->get('/Peserta/Kinerja', 'Peserta::kinerja');
$routes->get('/Peserta/Kinerja/Laporan/Bulanan', 'Peserta::bulanan');
$routes->get('/Peserta/Kinerja/Laporan/Harian', 'Peserta::harian');
$routes->get('/Peserta/Cetak/Nametag', 'Peserta::nametag');
$routes->get('/Peserta/Cetak/Sertifikat', 'Peserta::sertifikat');

//AUTH
$routes->get('/Login', 'Auth::login');
$routes->get('/Register', 'Auth::register');
$routes->post('/Register', 'Auth::add');
$routes->get('/Forgot', 'Auth::forgot');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}