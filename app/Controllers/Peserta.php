<?php

namespace App\Controllers;

use App\Models\DetailPesertaModel;
use App\Models\UserModel;
use App\Models\KinerjaModel;



class Peserta extends BaseController
{
    // CRUD data users on admin
    public function show()
    {
        $data = ['title' => 'Master Data Peserta'];
        return view('admin/page/FormDataPeserta/Peserta', $data);
    }

    public function create()
    {
        $data = ['title' => 'Master Data Peserta'];
        return view('admin/page/FormDataPeserta/Create', $data);
    }

    public function edit()
    {
        $data = ['title' => 'Master Data Peserta'];
        return view('admin/page/FormDataPeserta/Edit', $data);
    }

    //Enduser
    public function index()
    {
        $data = ['title' => 'Dashboard'];
        return view('enduser/page/index', $data);
    }

    // *Absensi
    public function absensi()
    {
        $data = ['title' => 'Absensi'];
        return view('enduser/page/absen/absen', $data);
    }

    //*Kinerja
    public function kinerja()
    {
        $data = ['title' => 'Kinerja'];
        return view('enduser/page/kinerja/kinerja', $data);
    }

    //Laporan Bulanan dan Harian
    public function bulanan()
    {
        $data = ['title' => 'Laporan Bulanan'];
        return view('enduser/page/kinerja/laporan_bulanan', $data);
    }

    public function harian()
    {
        $data = ['title' => 'Laporan Harian'];
        return view('enduser/page/kinerja/laporan_harian', $data);
    }

    //*Nametag
    public function nametag()
    {
        $data = ['title' => 'Nametag'];
        return view('enduser/page/cetak/nametag', $data);
    }

    public function sertifikat()
    {
        $data = ['title' => 'Sertifikat'];
        return view('enduser/page/cetak/sertifikat', $data);
    }
}