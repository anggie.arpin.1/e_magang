<?php

namespace App\Controllers;

use App\Models\MInstansiDinas;
use App\Models\MInstansiPeserta;
use App\Models\MUsers;

use App\Controllers\BaseController;

class Home extends BaseController
{
    public function __construct()
    {
        $this->InstansiDinas = new MInstansiDinas();
        $this->InstansiPeserta = new MInstansiPeserta();
        $this->User = new MUsers();
    }

    public function index()
    {
        $total_dinas = $this->InstansiDinas->tot_dinas();
        $total_sekolah = $this->InstansiPeserta->tot_sekolah();
        $total_peserta = $this->User->tot_user();
        $data = ['title' => '', 'mitra' => $total_sekolah, 'peserta' => $total_peserta, 'instansi' => $total_dinas];
        return view('landing/page/index', $data);
    }

    public function instansi()
    {
        $dinas = $this->InstansiDinas->findAll();
        $data = ['title' => '| Instansi', 'dinas' => $dinas];
        return view('landing/page/instansi', $data);
    }

    public function tentang()
    {
        $data = ['title' => '| Tentang'];
        return view('landing/page/about', $data);
    }

    public function profile()
    {
        $data = ['title' => 'Profile'];
        return view('admin/page/profile', $data);
    }
}