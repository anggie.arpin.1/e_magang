<?php

namespace App\Controllers;

use App\Models\MInstansiDinas;
use App\Models\MInstansiPeserta;
use App\Models\MUsers;

class Admin extends BaseController
{
    public function __construct()
    {
        $this->InstansiDinas = new MInstansiDinas();
        $this->InstansiPeserta = new MInstansiPeserta();
        $this->User = new MUsers();
    }
    public function index()
    {
        $total_dinas = $this->InstansiDinas->tot_dinas();
        $total_sekolah = $this->InstansiPeserta->tot_sekolah();
        $total_peserta = $this->User->tot_user();

        $data = ['title' => 'Dashboard', 'total_peserta' => $total_peserta, 'total_dinas' => $total_dinas, 'total_sekolah' => $total_sekolah];

        return view('admin/page/index', $data);
    }

    public function create()
    {
        $dinas = $this->InstansiDinas->findAll();

        $data = ['title' => 'Master Data Admin', 'dinas' => $dinas];
        return view('admin/page/FormDataAdmin/Create', $data);
    }

    public function show()
    {
        $data = ['title' => 'Master Data Admin'];
        return view('admin/page/FormDataAdmin/Admin', $data);
    }

    public function edit()
    {
        $data = ['title' => 'Master Data Admin'];
        return view('admin/page/FormDataAdmin/Edit', $data);
    }

    //Penilaian
    public function penilaian()
    {
        $data = ['title' => 'Penilaian Peserta'];
        return view('admin/page/Penilaian/Penilaian', $data);
    }
    public function kinerja()
    {
        $data = ['title' => 'Kinerja Peserta'];
        return view('admin/page/Penilaian/Kinerja', $data);
    }

    //Repository
    public function repository()
    {
        $data = ['title' => 'Repository Data'];

        return view('admin/page/Repository/Repository', $data);
    }
}