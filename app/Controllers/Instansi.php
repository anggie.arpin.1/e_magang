<?php

namespace App\Controllers;

use App\Models\MInstansiDinas;
use App\Models\MInstansiPeserta;

use App\Controllers\BaseController;
use phpDocumentor\Reflection\PseudoTypes\False_;

class Instansi extends BaseController
{

    public function __construct()
    {
        $this->InstansiPeserta = new MInstansiPeserta();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = ['title' => 'Master Data Instansi'];
        return view('admin/page/FormDataInstansi/Instansi', $data);
    }

    public function create()
    {
        $data = ['title' => 'Master Data Instansi'];
        return view('admin/page/FormDataInstansi/Create', $data);
    }

    public function edit()
    {
        $data = ['title' => 'Master Data Instansi'];
        return view('admin/page/FormDataInstansi/Edit', $data);
    }

    public function add()
    {



        $data = [

            'nama' => $this->request->getVar('nama'),
            'kd_instansi' => $this->request->getVar('kd_instansi'),
            'format' => $this->request->getVar('format'),
            'latitude' => $this->request->getVar('latitude'),
            'longitude' => $this->request->getVar('longitude'),
            'radius' => $this->request->getVar('radius'),
        ];

        return redirect()->to('/DataInstansi');
    }








    //Instansi Peserta
    public function instansiPeserta()
    {
        $instansi = $this->InstansiPeserta->findall();
        // dd($instansi);
        $data = ['title' => 'Master Data Instansi Peserta', 'instansi' => $instansi];
        return view('admin/page/FormDataInstansiPeserta/Instansi_Peserta', $data);
    }

    public function createPeserta()
    {
        $data = ['title' => 'Master Data Instansi Peserta'];
        return view('admin/page/FormDataInstansiPeserta/Create', $data);
    }

    public function editPeserta()
    {
        $data = ['title' => 'Master Data Instansi Peserta'];
        return view('admin/page/FormDataInstansiPeserta/Edit', $data);
    }

    public function addsekolah()
    {
        $builder = $this->db->table("instansi_peserta");

        //make uuid
        $builder->set('id_instansi_peserta', 'UUID()', FALSE);

        //make slug
        $nama = url_title($this->request->getVar('nama'), "-", true);
        $jurusan = url_title($this->request->getVar('jurusan'), "-", true);
        $slug = $nama . "_" . $jurusan;

        $data = [
            'nama_instansi' => $this->request->getVar('nama'),
            'jurusan' => $this->request->getVar('jurusan'),
            'slug' => $slug
        ];

        $add = $builder->insert($data);

        return redirect()->to('/DataInstansiPeserta')->with('success', 'Anda berhasil menambahkan data');
    }
}