<?php

namespace App\Controllers;

use App\Models\MInstansiDinas;
use App\Models\MInstansiPeserta;
use App\Models\MUsers;
use App\Models\MDetailPeserta;

use App\Controllers\BaseController;
use phpDocumentor\Reflection\Types\This;

class Auth extends BaseController
{
    public function __construct()
    {
        $this->InstansiDinas = new MInstansiDinas();
        $this->InstansiPeserta = new MInstansiPeserta();
        $this->User = new MUsers();
        $this->DetailPeserta = new MDetailPeserta();

        $this->db = \Config\Database::connect();
    }
    public function login()
    {
        $data = ['title' => '| Login'];
        return view('auth/page/login', $data);
    }

    public function forgot()
    {
        $data = ['title' => '| Lupa Password'];
        return view('auth/page/forgot', $data);
    }

    public function register()
    {
        $instansi = $this->InstansiDinas->findAll();
        $sekolah = $this->InstansiPeserta->findAll();

        $data = ['title' => '| Pendaftaran Magang', 'instansi' => $instansi, 'sekolah' => $sekolah];
        return view('landing/page/daftar', $data);
    }

    public function add()
    {
        //Insert data users for user table
        $builder = $this->db->table('users');
        //make uuid
        $user_uuid = $builder->set('id_users', 'UUID()', FALSE);
        dd($user_uuid);

        $data = [
            // 'id_users' => $user_uuid,
            'nama' => $this->request->getVar('nama'),
            'email' => $this->request->getVar('username'),
            'username' => $this->request->getVar('username'),
            'telepon' => $this->request->getVar('telepon'),
            'id_instansi_dinas' => $this->request->getVar('instansi_dinas'),
            'role' => "peserta",
        ];

        $builder->insert($data);


        // // //Insert data users for detail user table
        $builder = $this->db->table('detail_peserta');
        // // //make uuid
        $builder->set('id__detail_peserta', 'UUID()', FALSE);

        $data = [
            'id_users' => $user_uuid,
            'id_instansi_peserta' => $this->request->getVar('instansi_peserta'),
            'nomor_induk' => $this->request->getVar('nomor_induk'),
        ];
        // dd($data);
        $added = $builder->insert($data);

        // $uuid = [
        //     'uuid_user' => $user_uuid,
        //     'uuid_detail' => $detail_uuid
        // ];
        // dd($uuid);

        return redirect()->to('/Register')->with('success', 'Berhasil menambahkan Data');
    }
}