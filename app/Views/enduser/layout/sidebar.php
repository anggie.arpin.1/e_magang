<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/peserta" class="brand-link bg-teal">
        <img src="<?= base_url('assets/img/logo_kab.png') ?>" alt="Buleleng" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <b class="brand-text">E-Magang</b>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="/peserta" class="nav-link <?= ($title == 'Dashboard') ? 'active' : '' ?>">
                        <i class="fas fa-home nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <!-- Absensi -->
                <li class="nav-header">ABSENSI</li>
                <li class="nav-item">
                    <a href="/Peserta/Absensi" class="nav-link <?= ($title == 'Absensi') ? 'active' : '' ?>">
                        <i class="fas fa-book nav-icon"></i>
                        <p>Absensi</p>
                    </a>
                </li>


                <li class="nav-header">KINERJA</li>
                <li class="nav-item">
                    <a href="/Peserta/Kinerja" class="nav-link <?= ($title == 'Kinerja') ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Input Kinerja</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Laporan
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/Peserta/Kinerja/Laporan/Harian"
                                class="nav-link <?= ($title == 'Laporan Harian') ? 'active' : '' ?>">
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Harian</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/Peserta/Kinerja/Laporan/Bulanan"
                                class="nav-link <?= ($title == 'Laporan Bulanan') ? 'active' : '' ?>">
                                <i class="fas fa-angle-right nav-icon"></i>
                                <p>Bulanan</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header">CETAK</li>
                <li class="nav-item">
                    <a href="/Peserta/Cetak/Nametag" class="nav-link <?= ($title == 'Nametag') ? 'active' : '' ?>">
                        <i class="fas fa-address-card nav-icon"></i>
                        <p>Nametag</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/Peserta/Cetak/Sertifikat"
                        class="nav-link <?= ($title == 'Sertifikat') ? 'active' : '' ?>">
                        <i class="fas fa-certificate nav-icon"></i>
                        <p>Sertifikat</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>