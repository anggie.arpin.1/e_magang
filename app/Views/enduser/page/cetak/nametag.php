<?= $this->extend('enduser/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <div class="content mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h3>Selamat anda telah diterima Magang</h3>
                            <p>Silahkan cetak nametag anda dan selalu ingat gunakan nametag anda saat aktivitas magang
                                berlangsung. Untuk mencetak nametag silahkan klik tombol di bawah ini</p>
                            <a href="" class="btn btn-info"><i class="fas fa-id-card"></i> Cetak Nametag</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>