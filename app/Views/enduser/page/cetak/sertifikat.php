<?= $this->extend('enduser/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <div class="content mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body bg-success">
                                    <h3>Selamat anda telah dinyatakan lulus Magang</h3>
                                    <p>Terima kasih telah berkontribusi bersama kami dan telah melaksanakan magang
                                        dengan baik. Semoga pengalaman yang didapatkan ketika magang dapat bermanfaat
                                        bagi anda kedepannya.
                                    </p>
                                </div>
                            </div>
                            <p>Silahkan klik tombol di bawah ini untuk mencetak sertifikat.</p>
                            <a href="" class="btn btn-success"><i class="fas fa-certificate"></i> Cetak Sertifikat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>