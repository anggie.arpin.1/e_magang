<?= $this->extend('enduser/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Laporan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('/peserta') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Laporan Kinerja</li>
                        <li class="breadcrumb-item active">Bulanan</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row d-flex justify-content-around">
                                <div class="col-sm-10 col-12 mb-2">
                                    <form action="" method="Post" class="form-inline">
                                        <p class="mr-2 my-auto">Filter :</p>
                                        <div class="row">
                                            <div class="col col-xs-4">
                                                <select name="bulan" id="" class="form-control">
                                                    <option value="1">Januari</option>
                                                    <option value="2">Februari</option>
                                                    <option value="3">Maret</option>
                                                    <option value="4">April</option>
                                                    <option value="5">Mei</option>
                                                    <option value="6">Juni</option>
                                                    <option value="7">Juli</option>
                                                    <option value="8">Agustus</option>
                                                    <option value="9">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-outline-light d-inline"><i
                                                        class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-2 col-12 mb-2">
                                    <a href="" class="btn btn-info mb-2"><i class="fas fa-print"></i>
                                        Cetak
                                        Laporan</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="example2" class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Judul</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>13-07-2022</td>
                                            <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#modal-lg">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <form action="" method="POST" class="d-inline">
                                                    <button type="button" class="btn btn-danger"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Informasi Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-header">
                            <h5>Judul</h5>
                        </div>
                        <div class="card-body">
                            <P>Lorem, ipsum dolor sit amet consectetur adipisicing elit. </P>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5>Deskripsi</h5>
                        </div>
                        <div class="card-body">

                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut qui fugit voluptatibus
                                expedita
                                aperiam molestiae, labore, deleniti vero provident corrupti tempore consequatur ipsa
                                consectetur
                                laboriosam. Alias at dignissimos voluptate nam!</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5>Dokumentasi</h5>
                        </div>
                        <div class="card-body">
                            <img src="https://www.vinoti.com/wp-content/uploads/2021/12/Remote-work-wfh.jpg"
                                alt="Dokumentasi" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
    $('#delete').on('click', function() {
        Swal.fire({
            title: 'Anda Yakin?',
            text: "File ini akan terhapus",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Iya'
        })
    })
    </script>
    <?= $this->endSection(); ?>