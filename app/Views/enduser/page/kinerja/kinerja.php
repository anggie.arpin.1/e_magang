<?= $this->extend('enduser/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Kinerja</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('/peserta') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Input Kinerja</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="">Judul</label>
                                    <input type="text" name="judul" id="judul" class="form-control" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="">Deskripsi Kegiatan</label>
                                    <textarea name="deskripsi" id="deskripsi" rows="10" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Upload Dokumen Kegiatan</label>
                                    <input type="file" name="media" id="media" class="form-control">
                                </div>
                                <div class="form-group">
                                    <a href="<?= base_url('/peserta') ?>" class="btn btn-secondary">Batal</a>
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>