<?= $this->extend('auth/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="card-body">
    <h4 class="card-title text-center">Masuk</h4>
    <form method="POST" class="my-login-validation">
        <div class="form-group">
            <label for="text">Username/email</label>
            <input id="text" type="text" class="form-control" name="username" value="" required autofocus>
        </div>

        <div class="form-group">
            <label for="password">Password
                <a href="<?= base_url('/Forgot') ?>" class="float-right">
                    Lupa Password?
                </a>
            </label>
            <input id="password" type="password" class="form-control" name="password" required data-eye>
        </div>

        <div class="form-group m-0">
            <button type="submit" class="btn btn-primary btn-block">
                Login
            </button>
        </div>
        <div class="mt-4 text-center">
            <small><a href="https://kominfosanti.bulelengkab.go.id/" class="text-secondary" target="blank">Kominfosanti
                    &copy; <?= date('Y'); ?></a></small>
        </div>
    </form>
</div>

<script>
<?php if (session()->has("error")) { ?>
Swal.fire({
    icon: 'error',
    title: 'Login Gagal',
    text: 'Pastikan Email dan Password Benar',
    showConfirmButton: false,
    timer: 2000
})
<?php } ?>
</script>
<?= $this->endSection(); ?>