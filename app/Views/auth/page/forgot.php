<?= $this->extend('auth/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="card-body">
    <h4 class="card-title text-center">Lupa Password</h4>
    <form method="POST" class="my-login-validation" novalidate="">
        <div class="form-group">
            <label for="email">Username</label>
            <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
        </div>

        <div class="form-group m-0">
            <button type="submit" class="btn btn-primary btn-block">
                Kirim
            </button>
            <a href="<?= base_url('/Login') ?>" class="btn btn-outline-secondary btn-block">Kembali</a>
        </div>
    </form>
</div>

<script>
<?php if (session()->has("error")) { ?>
Swal.fire({
    icon: 'error',
    title: 'Konfirmasi Gagal',
    text: 'Pastikan email yang dimasukkan benar',
    showConfirmButton: false,
    timer: 2000
})
<?php } ?>

<?php if (session()->has("success")) { ?>
Swal.fire({
    icon: 'success',
    title: 'Email telah terkirim',
    text: 'Silahkan cek email anda',
    showConfirmButton: false,
    timer: 2000
})
<?php } ?>
</script>
<?= $this->endSection(); ?>