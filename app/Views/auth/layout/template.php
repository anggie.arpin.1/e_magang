<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-Magang | Masuk</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/dist/css/my-login.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/templatemo-lava.css') ?>">
</head>

<body class="my-login-page">
    <!-- Loader -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100">
                <div class="card-wrapper">
                    <div class="brand">
                        <img src="<?= base_url('assets/img/logo_kab.png') ?>" alt="Logo Kabupaten"
                            class="text-center img-fluid" width="100px">
                    </div>
                    <div class="card fat" data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                        <?= $this->renderSection('content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>

    <!-- Plugins -->
    <script src="<?= base_url('assets/dist/js/scrollreveal.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/waypoints.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/jquery.counterup.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/imgfix.min.js') ?>"></script>

    <!-- Global Init -->
    <script src="<?= base_url('assets/dist/js/custom.js') ?>"></script>
</body>

</html>