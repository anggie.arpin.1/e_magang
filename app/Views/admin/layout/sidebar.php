<!-- Sidebar -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/Admin" class="brand-link bg-teal">
        <img src="<?= base_url('assets/img/logo_kab.png') ?>" alt="Buleleng" class="brand-image" style="opacity: .8">
        <b class="brand-text">E-Magang</b>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="/Admin" class="nav-link <?= ($title == 'Dashboard') ? 'active' : '' ?>">
                        <i class="fas fa-home nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <!-- Master Data -->
                <li class="nav-header">MASTER DATA</li>
                <li class="nav-item">
                    <a href="<?= base_url('/DataAdmin') ?>"
                        class="nav-link <?= ($title == 'Master Data Admin') ? 'active' : '' ?>">
                        <i class="fas fa-user-cog nav-icon"></i>
                        <p>Data Admin</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('/DataInstansi') ?>"
                        class="nav-link <?= ($title == 'Master Data Instansi') ? 'active' : '' ?>">
                        <i class="fas fa-city nav-icon"></i>
                        <p>Data Instansi</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('/DataPeserta') ?>"
                        class="nav-link <?= ($title == 'Master Data Peserta') ? 'active' : '' ?>">
                        <i class="fas fa-users nav-icon"></i>
                        <p>Data Peserta</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('/DataInstansiPeserta') ?>"
                        class="nav-link <?= ($title == 'Master Data Instansi Peserta') ? 'active' : '' ?>">
                        <i class="fas fa-school nav-icon"></i>
                        <p>Data Instansi Peserta</p>
                    </a>
                </li>

                <!-- Penilaian -->
                <li class="nav-header">PENILAIAN MAGANG</li>
                <li class="nav-item">
                    <a href="<?= base_url('/Admin/Kinerja') ?>"
                        class="nav-link <?= ($title == 'Kinerja Peserta') ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-chart-bar"></i>
                        <p>Kinerja Magang</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('/Admin/Penilaian') ?>"
                        class="nav-link <?= ($title == 'Penilaian Peserta') ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>Nilai Peserta</p>
                    </a>
                </li>

                <!-- TTD Elektronik -->
                <li class="nav-header">TANDA TANGAN SERTIFIKAT</li>
                <li class="nav-item">
                    <a href="#" class="nav-link <?= ($title == 'Tanda Tangan') ? 'active' : '' ?>">
                        <i class="fas fa-pen-fancy nav-icon"></i>
                        <p>Tanda Tangan</p>
                    </a>
                </li>

                <!-- Repository Data -->
                <li class="nav-header">Repository</li>
                <li class="nav-item">
                    <a href="/Admin/Repository" class="nav-link <?= ($title == 'Repository Data') ? 'active' : '' ?>">
                        <i class="fas fa-trash-alt nav-icon"></i>
                        <p>Repository Data</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>