<?= $this->extend('admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Formulir Data Instansi Peserta</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Instansi Peserta</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Nama Sekolah/Universitas</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                        placeholder="Nama Sekolah atau Universitas" value="Nama Sekolah atau Univ Lama"
                                        autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Jurusan</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                        placeholder="Nama Jurusan" value="Nama Jurusan Lama">
                                </div>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio1" value="option1" checked>
                                        <label class="form-check-label" for="inlineRadio1">Aktif</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                            id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">Non Aktif</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="/DataInstansiPeserta" class="btn btn-secondary">Batal</a>
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
<?= $this->endSection(); ?>