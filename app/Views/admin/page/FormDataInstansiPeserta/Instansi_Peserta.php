<?= $this->extend('admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">

    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Data Instansi Peserta</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Instansi Peserta</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <a href="/Create/DataInstansiPeserta" class="btn btn-success mb-2"><i
                                    class="fas fa-plus-circle"></i>
                                Tambah Data</a>
                            <div class="table-responsive">
                                <table id="example2" class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-1">No.</th>
                                            <th>Nama Sekolah/Universitas</th>
                                            <th>Jurusan</th>
                                            <th class="col-1">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($instansi as $instasis) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $instasis['nama_instansi']; ?></td>
                                            <td><?= $instasis['jurusan']; ?></td>
                                            <td>
                                                <a href="/Edit/DataInstansiPeserta" class="btn btn-warning"><i
                                                        class="far fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>

<script>
$('#delete').on('click', function() {
    Swal.fire({
        title: 'Anda Yakin?',
        text: "File ini akan terhapus",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya'
    })
})

<?php if (session()->has("success")) { ?>

<?php } ?>
</script>
<?= $this->endSection(); ?>