<?= $this->extend('admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">

    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Peserta Magang</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Peserta Magang</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- <a href="/Create/DataPeserta" class="btn btn-success mb-2"><i
                                    class="fas fa-plus-circle"></i>
                                Tambah Data</a> -->
                            <div class="table-responsive">
                                <table id="example2" class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>NIS/NIM</th>
                                            <th>Telepon</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>I Gede Anggie Suardika Arpin</td>
                                            <td>1915091008</td>
                                            <td>082237256677</td>
                                            <td>
                                                <p class="btn btn-success btn-xs">Aktif</p>
                                            </td>
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal"
                                                    data-target="#modal-lg"><i class="fas fa-eye"></i></button>
                                                <form action="" method="POST" class="d-inline">
                                                    <button type="button" class="btn btn-danger" id="delete1"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>2</td>
                                            <td>ASU</td>
                                            <td>1915091066</td>
                                            <td>08223677</td>
                                            <td>
                                                <p class="btn btn-danger btn-xs">Non-Aktif</p>
                                            </td>
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal"
                                                    data-target="#modal-lg"><i class="fas fa-eye"></i></button>
                                                <form action="" method="POST" class="d-inline">
                                                    <button type="button" class="btn btn-danger" id="delete2"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- modal -->
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Informasi Peserta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="<?= base_url('/assets/img/avatar.png') ?>" alt="account" class="img-fluid mb-3"
                        width="150px">
                    <div class="card">
                        <div class="card-header">
                            <h6>Identitas</h6>
                        </div>
                        <div class="card-body">
                            <ol>
                                <li>Nama : Nama Peserta</li>
                                <li>Email : Email Peserta</li>
                                <li>Alamat : Alamat Peserta</li>
                            </ol>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Status
                        </div>
                        <div class="card-body">
                            <a href="" class="btn btn-success">Aktif</a>
                            <a href="" class="btn btn-danger"> Non-Aktif</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>

<script>
$('#delete1').on('click', function() {
    Swal.fire({
        title: 'Anda Yakin?',
        text: "File ini akan terhapus",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya'
    })
})
<?php if (session()->has('success')) { ?>
Swal.fire({
    icon: 'success',
    title: 'Berhasil',
    text: 'Anda Telah Mengaktifkan Data Peserta',
    showConfirmButton: false,
    timer: 2000
})
<?php } ?>
</script>
<?= $this->endSection(); ?>