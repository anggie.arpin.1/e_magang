<?= $this->extend('admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Penilaian Peserta Magang</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Peserta</li>
                        <li class="breadcrumb-item active">Penilaian</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-light" role="alert">
                        <i class="fas fa-info-circle mr-2"></i>
                        <p class="d-inline">Masukkan nilai sesuai dengan kriteria yang diberikan.</p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example2" class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>Asal</th>
                                            <th>Instansi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>I Gede Anggie Suardika Arpin</td>
                                            <td>Universitas Pendidikan Ganesha</td>
                                            <td>Dinas Kominfosanti</td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#modal-lg">
                                                    <i class="far fa-edit mr-1"></i> Nilai
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>2</td>
                                            <td>I Putu Bastian Adi Putra</td>
                                            <td>Universitas Pendidikan Ganesha</td>
                                            <td>Dinas Kominfosanti</td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#modal-lg">
                                                    <i class="far fa-edit mr-1"></i> Nilai
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penilaian</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-3">
                            <img src="<?= base_url('/assets/img/avatar.png') ?>" alt="Avatar" class="img-fluid">
                        </div>
                        <div class="col-9">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="">Nama Mahasiswa</label>
                                    <input type="text" name="" id="" class="form-control" placeholder="Nama Mahasiswa"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Nilai Kehadiran</label>
                                    <input type="number" name="" id="" class="form-control" placeholder="89" disabled>
                                    <small id="nilai" class="form-text text-muted">Nilai yang dimasukkan dalam skala
                                        0-10</small>
                                </div>
                                <div class="form-group">
                                    <label for="">Nilai Sikap</label>
                                    <input type="number" name="" id="" class="form-control" placeholder="89">
                                    <small id="nilai" class="form-text text-muted">Nilai yang dimasukkan dalam skala
                                        0-100</small>
                                </div>
                                <div class="form-group">
                                    <label for="">Nilai Project</label>
                                    <input type="number" name="" id="" class="form-control" placeholder="89">
                                    <small id="nilai" class="form-text text-muted">Nilai yang dimasukkan dalam skala
                                        0-100</small>
                                </div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-success" id="save">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>

<script>
$('#save').on('click', function() {
    Swal.fire({
        title: 'Yakin Menyimpan Nilai ?',
        text: "Nilai yang dimasukkan ke dalam sistem.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya'
    })
})

<?php if (session()->has("success")) { ?>
Swal.fire({
    icon: 'success',
    title: 'Berhasil',
    text: 'Anda Telah Menambahkan Nilai Peserta',
    showConfirmButton: false,
    timer: 2000
})
<?php } ?>
</script>
<?= $this->endSection(); ?>