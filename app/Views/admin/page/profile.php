<?= $this->extend('/admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Informasi Profil</h1>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Informasi Profile</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penilaian</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-3">
                            <img src="<?= base_url('/assets/img/avatar.png') ?>" alt="Avatar" class="img-fluid">
                        </div>
                        <div class="col-9">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="">Nama Mahasiswa</label>
                                    <input type="text" name="" id="" class="form-control" placeholder="Nama Mahasiswa"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Nilai Mahasiswa</label>
                                    <input type="number" name="" id="" class="form-control" placeholder="0-10">
                                    <small id="nilai" class="form-text text-muted">Nilai yang dimasukkan dalam skala
                                        0-10</small>
                                </div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-success" id="save">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<?= $this->endSection(); ?>