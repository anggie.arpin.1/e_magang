<?= $this->extend('/admin/layout/template'); ?>

<?= $this->section('content'); ?>
<div class="content-wrapper">
    <!-- Header Content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Repository Data</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Repository</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Admin</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-1">No.</th>
                                            <th class="col-6">Nama</th>
                                            <th class="col-4">Username</th>
                                            <th class="col-1">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>Mr.God</td>
                                            <td>mrgod</td>
                                            <td>
                                                <a href="" class="btn btn-info"><i class="fas fa-reply"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Instansi</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-1">No.</th>
                                            <th class="col-6">Nama Instansi</th>
                                            <th class="col-4">Kode</th>
                                            <th class="col-1">Restore</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>Dinas Komunikasi, Informatika, Persandian, dan Statistik</td>
                                            <td>kominfosanti0362 <i>Example</i></td>
                                            <td>
                                                <a href="/Edit/DataInstansi" class="btn btn-info"><i
                                                        class="fas fa-reply"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Peserta</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example2" class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-1">No.</th>
                                            <th class="col-6">Nama</th>
                                            <th class="col-4">NIS/NIM</th>
                                            <th class="col-1">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- Loop Data Tables -->
                                            <td>1</td>
                                            <td>I Gede Anggie Suardika Arpin</td>
                                            <td>1915091008</td>
                                            <td>
                                                <a href="" class="btn btn-info"><i class="fas fa-reply"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penilaian</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-3">
                            <img src="<?= base_url('/assets/img/avatar.png') ?>" alt="Avatar" class="img-fluid">
                        </div>
                        <div class="col-9">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="">Nama Mahasiswa</label>
                                    <input type="text" name="" id="" class="form-control" placeholder="Nama Mahasiswa"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Nilai Mahasiswa</label>
                                    <input type="number" name="" id="" class="form-control" placeholder="0-10">
                                    <small id="nilai" class="form-text text-muted">Nilai yang dimasukkan dalam skala
                                        0-10</small>
                                </div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-success" id="save">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<?= $this->endSection(); ?>