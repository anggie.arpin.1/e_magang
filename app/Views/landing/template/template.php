<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <title>E-Magang <?= $title; ?></title>

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css"
        integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="<?= base_url('assets/dist/css/templatemo-lava.css') ?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css"
        integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css"
        integrity="sha512-OTcub78R3msOCtY3Tc6FzeDJ8N9qvQn1Ph49ou13xgA9VsH9+LRxoFU6EqLhW4+PKRfU+/HReXmSZXHEkpYoOA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body>

    <!-- Loader -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>


    <!-- Navbar -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav nav-left">
                        <a href="<?= base_url('/') ?>" class="logo">
                            <img src="<?= base_url('assets/img/brand.png') ?>" alt="" class="responsive-logo">
                        </a>
                        <ul class="nav nav-right">
                            <li class="scroll-to-section"><a href="<?= base_url('/') ?>"
                                    class="<?= ($title == '') ? 'text-primary' : '' ?>">Beranda</a>
                            </li>
                            <li class="scroll-to-section"><a href="<?= base_url('/About') ?>"
                                    class="<?= ($title == '| Tentang') ? 'text-primary' : '' ?>">Tentang</a></li>
                            <li class="scroll-to-section"><a href="<?= base_url('/Instansi') ?>"
                                    class="<?= ($title == '| Instansi') ? 'text-primary' : '' ?>">Instansi</a>
                            <li class=" scroll-to-section"><a href="#footer">Kontak</a></li>
                            </li>
                            <li class="navbar-button"><a href="/Login" class="link-navbar">
                                    <p class="py-0 paragraf-link">Masuk</p>
                                </a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <?= $this->renderSection('content'); ?>

    <!-- Footer -->
    <footer class="bg-dark footer" id="footer">
        <div class="container text-center text-md-left text-light">
            <div class="row">
                <div class="col-md-4 mx-auto mb-3">
                    <img src="<?= base_url('assets/img/brand-footer.png') ?>" alt="Logo E-Magang" class="img-footer">
                    <p class="text-footer">E-Magang membantu Kantor Dinas/Instansi Pemerintahan di Kabupaten Buleleng
                        untuk melakukan administrasi
                        dan pengelolaan peserta magang secara digital.
                    </p>
                    <ul class="social">
                        <li><a href="https://www.facebook.com/kominfosanti.bulelengkab" target="blank"><i
                                    class="fa-brands fa-facebook fa-lg"></i></a></li>
                        <li><a href="https://www.instagram.com/kominfosanti.buleleng" target="blank"><i
                                    class="fa-brands fa-instagram fa-lg"></i></a></li>
                        <li><a href="https://www.tiktok.com/@kominfosanti.buleleng01" target="blank"><i
                                    class="fa-brands fa-tiktok fa-lg"></i></a></li>
                    </ul>
                </div>

                <div class="col-md-3 mx-auto mt-2">
                    <h5 class="textuppercase font-weight-bold">Halaman</h5>
                    <hr class="bg-primary mb-1 mt-2 d-inline-block mx-auto" style="width: 110px; height: 2px">

                    <ul class="list-unlisted">
                        <li class="my-2"><a href="<?= base_url('/') ?>" class="text-link">Beranda</a></li>
                        <li class="my-2"><a href="<?= base_url('/') ?>" class="text-link">Tentang</a></li>
                        <li class="my-2"><a href=" <?= base_url('/Instansi') ?>" class="text-link">Instansi</a></li>
                    </ul>
                </div>

                <div class="col-md-3 mx-auto mt-2">
                    <h5 class="textuppercase font-weight-bold">Lainnya</h5>
                    <hr class="bg-primary mb-1 mt-2 d-inline-block mx-auto" style="width: 110px; height: 2px">

                    <ul class="list-unlisted">
                        <li class="my-2"><a href="<?= base_url('/Register') ?>" class="text-link">Daftar</a></li>
                        <li class="my-2"><a href="<?= base_url('/Login') ?>" class="text-link">Masuk</a></li>
                    </ul>
                </div>

            </div>

        </div>
        <div class="footer-copyright text-center py-3">
            <hr>
            <p class="text-light"> Copyright <?= date('Y'); ?> &copy;
                <a href="https://kominfosanti.bulelengkab.go.id/" target="_blank" class="text-light"> Kominfosanti</a>
            </p>
        </div>
    </footer>

    <!-- Sweet Alert 2 -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>

    <!-- Plugins -->
    <script src="<?= base_url('assets/dist/js/scrollreveal.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/waypoints.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/jquery.counterup.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/imgfix.min.js') ?>"></script>

    <!-- Global Init -->
    <script src="<?= base_url('assets/dist/js/custom.js') ?>"></script>
    <script>
    $(function() {
        <?php if (session()->has("success")) { ?>
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: '<?= session("success") ?>',
            showConfirmButton: false,
            timer: 1500
        })
        <?php } ?>
    });
    </script>
</body>

</html>