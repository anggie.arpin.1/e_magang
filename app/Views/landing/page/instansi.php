<?= $this->extend('landing/template/template'); ?>

<?= $this->section('content'); ?>
<section class="section" id="hero">
    <div class="header-text">
        <div class="container" data-scroll-reveal="enter top move 30px over 0.6s after 0.4s">
            <div class="text-center text-light d-flex flex-column h-100 justify-content-center">
                <h1 class="text-uppercase">Daftar Instansi</h1>
                <h6 class="mt-2">Berbagai instansi telah membuka lowongan magang untuk para siswa dan mahasiswa</h6>
            </div>
        </div>
    </div>
</section>

<section class="section" id="instansi">
    <div class="container-instansi">
        <div class="row">
            <?php foreach ($dinas as $datadinas) : ?>
            <div class="col-md-6 col-lg-3  mb-3" data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                <div class=" card">
                    <img src="https://asset.balipuspanews.com/wp-content/uploads/2021/12/20180847/IMG-20211220-WA0029.jpg"
                        class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?= $datadinas['nama_dinas']; ?></h5>
                        <p class="card-text"><?= $datadinas['alamat']; ?></p>
                        <a href="<?= $datadinas['link_web']; ?>" class="btn btn-primary mt-2 d-block text-center"
                            target="blank"><i class="fas fa-globe"></i>
                            <p class="d-inline text-white">Website
                                Resmi</p>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>