<?= $this->extend('landing/template/template'); ?>

<?= $this->section('content'); ?>
<section class="section" id="hero">
    <div class="header-text">
        <div class="container" data-scroll-reveal="enter top move 30px over 0.6s after 0.4s">
            <div class="text-center text-light d-flex flex-column h-100 justify-content-center">
                <h1 class="text-uppercase">Pendaftaran Magang</h1>
                <h6 class="mt-2">Ayo jangan lewatkan kesempatan ini, mari daftar magang sekarang</h6>
            </div>
        </div>
    </div>
</section>

<!-- ***** Features Big Item Start ***** -->
<section class="section" id="instansi">
    <div class="container-instansi">
        <div class="alert alert-info text-center" role="alert">
            <i class="fa-solid fa-circle-info"></i>
            <p class=" d-inline "> <b>Perhatian : </b> Isi formulir di bawah ini sesuai dengan informasi pribadi
                anda. Pastikan data yang anda kirimkan adalah data yang valid.</p>
        </div>
        <div class="card">
            <div class="card-header bg-white">
                <a href="/" class="text-secondary"> <i class="fas fa-arrow-alt-circle-left mr-2"></i>Kembali</a>
                <h4 class="my-2">Daftar Magang</h4>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="row mt-3">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="Nama Lengkap">Nama Lengkap</label>
                                <input type="text" name="nama" id="" class="form-control" required>
                                <small class="form-text text-muted"><span class="text-danger">*</span>Nama
                                    sesuai ijazah</small>
                            </div>
                            <div class="form-group">
                                <label for="Nama Lengkap">Nomor Induk</label>
                                <input type="text" name="nomor_induk" id="" class="form-control" required>
                                <small class="form-text text-muted"><span class="text-danger">*</span>NISN/NIM</small>
                            </div>
                            <div class="form-group">
                                <label for="Nama Lengkap">Nomor Telepon</label>
                                <input type="text" name="telepon" id="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Email"> Foto</label>
                                <input type="file" name="foto" id="" class="form-control">
                                <small class="form-text text-muted"><span class="text-danger">*</span>Ukuran
                                    maksimal 2MB</small>
                            </div>
                            <div class="form-group">
                                <label for="Email"> Email</label>
                                <input type="email" name="email" id="" class="form-control" required>
                                <small class="form-text text-muted"><span class="text-danger">*</span>Disarankan
                                    menggunakan email dari sekolah/univ</small>
                            </div>
                            <div class="form-group">
                                <label for="Email"> Username</label>
                                <input type="text" name="username" id="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="Nama Lengkap">Asal Sekolah/Universitas dan Jurusan</label>
                                <select name="instansi_peserta" id="instansi_peserta" class="form-control" required>
                                    <option>Nama Sekolah - Jurusan</option>
                                    <?php foreach ($sekolah as $sekolahs) : ?>
                                    <option value="<?= $sekolahs['id_instansi_peserta']; ?>">
                                        <?= $sekolahs['nama_instansi'] . " - " . $sekolahs['jurusan']; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Nama Lengkap">Sekolah/Universitas</label>
                                <input type="text" name="nama" id="" class="form-control" required>
                                <small class="form-text text-muted"><span class="text-danger">*</span>Ini diisi
                                    apabila sekolah/universitas ataupun jurusan belum terdaftar</small>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Nama Lengkap">Jurusan Lainnya</label>
                                <input type="text" name="nama" id="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="Nama Lengkap">Tempat Magang</label>
                                <select name="instansi_dinas" id="" class="form-control" required>
                                    <option value="">Pilih Dinas</option>
                                    <?php foreach ($instansi as $instansis) : ?>
                                    <option value="<?= $instansis['id_instansi_dinas']; ?>">
                                        <?= $instansis['nama_dinas']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Nama Lengkap">Password</label>
                                <input type="text" name="nama" id="" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="Nama Lengkap">Konfirmasi Password</label>
                                <input type="text" name="nama" id="" class="form-control" required>
                                <small class="form-text text-muted"><span class="text-danger">*</span>Ketik ulang
                                    password</small>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-3">
                            <button type="submit" class="form-control btn btn-success">Daftar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>