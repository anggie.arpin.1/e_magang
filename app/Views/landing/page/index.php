<?= $this->extend('landing/template/template'); ?>

<?= $this->section('content'); ?>
<div class="welcome-area" id="welcome">
    <div class="header-text">
        <div class="container ">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <h1><em>E-Magang</em></h1>
                    <p>E-Magang adalah sistem informasi manajemen berbasis website yang membantu Dinas
                        dan Instansi Pemerintahan Kabupaten Buleleng untuk melakukan administrasi dan pengelolaan
                        Mahasiswa dan Siswa Magang menjadi lebih efektif dan efisien tanpa harus melakukan
                        perekapan data secara konvensional</p>
                    <a href="<?= base_url('/Register') ?>" class="main-button-slider mb-4">Daftar Sekarang <i
                            class="fa-solid fa-circle-arrow-right ms-4"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section" id="data">
    <div class="container mt-4">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"
                data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <div class="features-item">
                    <div class="features-icon">

                        <p class="h1 text-primary"><?= $mitra; ?></p>
                        <h6>Mitra</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"
                data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                <div class="features-item">
                    <div class="features-icon">
                        <p class="h1 text-primary"><?= $peserta; ?></p>
                        <h6>Peserta Magang</h6>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"
                data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                <div class="features-item">
                    <div class="features-icon">
                        <p class="h1 text-primary"><?= $instansi; ?></p>
                        <h6>Instansi</h6>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section" id="manfaat">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="center-heading" data-scroll-reveal="enter top move 30px over 0.6s after 0.4s">
                    <h2 class="text-uppercase"><em>Manfaat</em></h2>
                    <p>Banyak manfaat yang kita dapatkan magang di instansi pemerintah kabupaten Buleleng</p>
                </div>
            </div>
            <div class="left-image col-lg-6 col-md-12 col-sm-12 mobile-bottom-fix-big"
                data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <img src="<?= base_url('assets/img/leftimage.png') ?>" class="img-fluid d-block mx-auto" alt="png">
            </div>
            <div class="right-text col-lg-6 col-md-12 col-sm-12 mobile-bottom-fix">
                <ul>
                    <li data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="assets/img/about1.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                                <div class="text">
                                    <h4>Mendapatkan Pengalaman Kerja</h4>
                                    <p>Magang adalah salah satu cara untuk mendapatkan pengalaman sebelum benar-benar
                                        terjun ke dunia kerja secara full time.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-scroll-reveal="enter right move 30px over 0.6s after 0.5s">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="assets/img/about2.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                                <div class="text">
                                    <h4>Mengetahui Lingkungan Kerja</h4>
                                    <p>Bisa belajar menyesuaikan diri di lingkungan
                                        dinas pemerintahan sangat diperlukan nantinya baik dunia kerja.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-scroll-reveal="enter right move 30px over 0.6s after 0.6s">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="assets/img/about3.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                                <div class="text">
                                    <h4>Menambah Skill atau Keterampilan</h4>
                                    <p>Melatih kemampuan berkomunikasi dengan baik dengan rekan kerja.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section" id="testimonials">
    <div class="container-landing">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="center-heading" data-scroll-reveal="enter top move 30px over 0.6s after 0.4s">
                    <h2 class="text-uppercase"><em>Daftar Mitra</em></h2>
                    <p>Daftar sekolah dan universitas yang telah bermitra dalam pelaksanaan magang di instansi kabupaten
                        Buleleng</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12" data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                <section class="container">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/UNDIKSHA.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">Universitas Pendidikan Ganesha</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/STM.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">SMK Negeri 3 Singaraja</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/STAH.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">STAH Mpu Kuturan</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/SMEA.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">SMK Negeri 1 Singaraja</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/SMK1Sawan.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">SMK Negeri 1 Sawan</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/SMK1Sukasada.png') ?>" alt=""
                                    class="text-center" width="50px">
                                <h6 class="text-center mt-2">SMK Negeri 1 Sukasada</h6>
                            </div>
                            <div class="item">
                                <img src="<?= base_url('assets/img/mitra/TIGlobal.png') ?>" alt="" class="text-center"
                                    width="50px">
                                <h6 class="text-center mt-2">SMK TI GLobal Singaraja</h6>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>