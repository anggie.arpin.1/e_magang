<?= $this->extend('landing/template/template'); ?>

<?= $this->section('content'); ?>
<section class="section" id="hero">
    <div class="header-text">
        <div class="container" data-scroll-reveal="enter top move 30px over 0.6s after 0.4s">
            <div class="text-center text-light d-flex flex-column h-100 justify-content-center">
                <h1 class="text-uppercase">Tentang</h1>
                <h6 class="mt-2">Informasi mengenai E-Magang</h6>
            </div>
        </div>
    </div>
</section>

<section class="section" id="Tentang">
    <div class="container-about">
        <div class="row">
            <div class="col-lg-5 col-md-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <img src="<?= base_url('assets/img/web.png') ?>" alt="" class="img-fluid mx-auto">
            </div>
            <div class=" col-lg-7 col-md-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                <h2 class="header-judul text-primary">Tentang E-Magang</h2>
                <p class="mt-3">E-Magang merupakan sistem informasi berbasis website yang digunakan untuk membantu
                    Kantor
                    Dinas/Instansi Pemerintahan di Kabupaten Buleleng untuk melakukan administrasi dan pengelolaan
                    peserta magang menjadi lebih efektif dan efisien karena sistem ini akan membuat segala kegiatan yang
                    sebelumnya dilakukan secara konvensional menjadi dilakukan secara digital.</p>
            </div>
        </div>
    </div>
</section>

<section class="section" id="features">
    <div class="container-about">
        <div class="row">
            <div class=" col-lg-7 col-md-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <h2 class="header-judul text-primary">Fitur-fitur</h2>
                <p class="mt-3">Adapun fitur-fitur yang terdapat pada sistem informasi ini adalah :</p>
                <ul>
                    <li class="mt-3"><img src="<?= base_url('assets/img/dot.png') ?>" alt="" width="15px"
                            class="mx-2"><b>Absensi Peserta Magang</b>, <p class="d-inline">fitur yang digunakan oleh
                            peserta magang untuk melakukan absensi sesuai dengan lokasi tempat magang.</p>
                    </li>
                    <li class="mt-3"><img src="<?= base_url('assets/img/dot.png') ?>" alt="" width="15px"
                            class="mx-2"><b>Kinerja Magang</b>, <p class="d-inline"> fitur yang digunakan oleh peserta
                            magang untuk melihat dan mengetahui kinerja dari peserta magang.</p>
                    </li>
                    <li class="mt-3"><img src="<?= base_url('assets/img/dot.png') ?>" alt="" width="15px"
                            class="mx-2"><b>Laporan Kegiatan Magang</b>, <p class="d-inline">fitur yang digunakan oleh
                            peserta magang untuk mengunggah laporan kegiatan.</p>
                    </li>
                    <li class="mt-3"><img src="<?= base_url('assets/img/dot.png') ?>" alt="" width="15px"
                            class="mx-2"><b>Cetak Sertifikat Magang</b>, <p class="d-inline">fitur yang digunakan oleh
                            peserta magang untuk mencetak sertifikat magang.</p>
                    </li>
                    <li class="mt-3 mb-5"><img src="<?= base_url('assets/img/dot.png') ?>" alt="" width="15px"
                            class="mx-2"><b>Cetak Nametag</b>, <p class="d-inline">fitur yang digunakan oleh peserta
                            magang
                            untuk mencetak nametag.</p>
                    </li>
                </ul>
            </div>
            <div class=" col-lg-5">
                <div class="container" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="<?= base_url('assets/img/features.png') ?>" alt="Animasi" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<div class="start-instansi"></div>


<?= $this->endSection(); ?>