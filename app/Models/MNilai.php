<?php

namespace App\Models;

use CodeIgniter\Model;

class MNilai extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'nilai';
    protected $primaryKey           = 'id_nilai';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "nilai_kehadiran",
        "nilai_sikap",
        "nilai_project",
        "total",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}