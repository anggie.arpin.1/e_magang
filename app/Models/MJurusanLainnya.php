<?php

namespace App\Models;

use CodeIgniter\Model;

class MJurusanLainnya extends Model
{
    protected $table                = 'jurusan_lainnya';
    protected $primaryKey           = 'id_jurusan_lainnya';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "nama_instansi",
        "jurusan",
        "slug",
        "status",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}