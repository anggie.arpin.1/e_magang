<?php

namespace App\Models;

use CodeIgniter\Model;

class MDetailPeserta extends Model
{
    protected $table                = 'detail_peserta';
    protected $primaryKey           = 'id_detail_peserta';
    protected $useSoftDelete        = true;
    protected $allowedFields        = [
        "telepon",
        "nomor_induk",
        "status",
        "file_photo",
        "tgl_mulai",
        "tgl_selesai",
        "status_magang",
    ];
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}