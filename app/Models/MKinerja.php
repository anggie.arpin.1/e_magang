<?php

namespace App\Models;

use CodeIgniter\Model;

class MKinerja extends Model
{
    protected $table                = 'kinerja';
    protected $primaryKey           = 'id_kinerja';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "tanggal_kegiatan",
        "judul_kegiatan",
        "uraian_kegiatan",
        "file",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}