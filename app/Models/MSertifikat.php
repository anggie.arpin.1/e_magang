<?php

namespace App\Models;

use CodeIgniter\Model;

class MSertifikat extends Model
{
    protected $table                = 'sertifikat';
    protected $primaryKey           = 'id_sertifikat';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "nomor_surat",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}