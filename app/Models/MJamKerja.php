<?php

namespace App\Models;

use CodeIgniter\Model;

class MJamKerja extends Model
{
    protected $table                = 'jam_kerja';
    protected $primaryKey           = 'id_jam_kerja';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "wkt_mulai_pagi",
        "wkt_selesai_pagi",
        "wkt_mulai_sore",
        "wkt_selesai_sore",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}