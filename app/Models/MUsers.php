<?php

namespace App\Models;

use CodeIgniter\Model;

class MUsers extends Model
{
    protected $table                = 'users';
    protected $primaryKey           = 'id_user';
    protected $returnType           = 'array';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "email",
        "password",
        "username",
        "nama",
        "NIP",
        "telepon",
        "role"
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    public function tot_user()
    {
        return $this->db->table('users')->countAll();
    }
}