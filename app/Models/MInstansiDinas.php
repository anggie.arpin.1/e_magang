<?php

namespace App\Models;

use CodeIgniter\Model;

class MInstansiDinas extends Model
{
    protected $table                = 'instansi_dinas';
    protected $primaryKey           = 'id_instansi_dinas';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "nama_dinas",
        "kode_dinas",
        "alamat",
        "foto_gedung",
        "link_web",
        "longitude",
        "lotitude",
        "radius",
        "format_surat",
    ];
    // Dates 
    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    public function tot_dinas()
    {
        return $this->db->table('instansi_dinas')->countAll();
    }
}