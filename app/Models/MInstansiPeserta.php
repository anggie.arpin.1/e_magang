<?php

namespace App\Models;

use CodeIgniter\Model;

class MInstansiPeserta extends Model
{
    protected $table                = 'instansi_peserta';
    protected $primaryKey           = 'id_instansi_peserta';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "nama_instansi",
        "jurusan",
        "slug",
        "status"
    ];

    protected $useTimestamps        = true;
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    public function tot_sekolah()
    {
        return $this->db->table('instansi_peserta')->countAll();
    }

    public function insert_sekolah()
    {

        return 0;
    }
}