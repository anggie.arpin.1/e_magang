<?php

namespace App\Models;

use CodeIgniter\Model;

class MAbsensi extends Model
{

    protected $table                = 'absensi';
    protected $primaryKey           = 'id_absensi';
    protected $useSoftDelete        = true;
    protected $protectFields        = true;
    protected $allowedFields        = [
        "tgl_absen",
        "wkt_absen",
        "status",

    ];
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';
}